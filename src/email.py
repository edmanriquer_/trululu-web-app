from smtplib import *
from xml.dom.pulldom import parseString
from typing import *
from email.mime.multipart import *
from email.mime.text import *
from email.mime.base import *
from email import *
from email import encoders
from flask import session
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import *
from reportlab.lib.utils import ImageReader
from datetime import datetime
import time
import smtplib

class Email:
    def __init__(self):
        self.server = SMTP(
            host='smtp.gmail.com',
            port=587
        )

    def connect_server(self):
        self.server.starttls()
        self.server.login(
            user='trululucr@gmail.com',
            password='tiendatrululucr020518*-'
        )

    def send_email(self, emails, subject, message_format, format):
        self.connect_server()
        for email in emails:
            print('Se conectó')
            mime = MIMEMultipart()
            mime['From'] = 'trululucr@gmail.com'
            mime['To'] = email
            mime['Subject'] = subject
            format = MIMEText(message_format, format)
            mime.attach(format)
            try:
                self.server.sendmail('trululucr@gmail.com', email, mime.as_string())
            except Exception as e:
                print('666')
                raise e
            finally:
                self.disconnect_server()

    def disconnect_server(self):
        self.server.quit()
        self.server.close()

    def make_pdf(self, clave, receptor):
        # Creación del PDF
        c = canvas.Canvas('factura.pdf', pagesize=letter)

        # Formato del PDF
        c.setFont("Times-Roman", 12)
        c.setLineWidth(.3)

        # Contenido del PDF
        c.drawString(30, 750, 'Eduardini el invencible')
        c.drawString(30, 735, 'Jhonnyleo')
        c.drawString(500, 750, 'Estemelcochon')
        c.line(480, 747, 580, 747)

        c.drawString(275, 725, 'Estimado')
        c.drawString(500, 725, 'usuario de Tienda Trululu')
        c.line(378, 723, 580, 723)

        # Guardar el PDF
        c.showPage()
        c.save()

        time.sleep(1)

        # Generación del correo
        asunto = 'Pedido realizado exitosamente'
        cuerpo = ''
        ruta_archivo = ''
        nombre_archivo = ''

        mime = MIMEMultipart()
        mime['From'] = 'trululucr@gmail.com'
        mime['To'] = receptor
        mime['Subject'] = asunto
        format = MIMEText(cuerpo, 'plain')
        mime.attach(format)

    def genera_factura(self, id, nombre, apellido, provincia, canton, direccion_detallada, telefono, correo, productos, total):

        # Creación del PDF
        c = canvas.Canvas('factura.pdf', pagesize=letter)

        # Fecha actual
        today = datetime.now()

        # Formato del PDF
        c.setFont("Times-Roman", 12)
        c.setLineWidth(.3)

        # Contenido del PDF

        # Imagen
        logo = ImageReader('https://scontent.fsjo15-1.fna.fbcdn.net/v/t1.15752-9/275321191_745894256328360_6605478899883696543_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=ae9488&_nc_ohc=HnLULISitjcAX-DNKsy&_nc_ht=scontent.fsjo15-1.fna&oh=03_AVKWhAPv9Lya-5LFTcrq6HAj3EFWcR405_xR6OAs3ThAbw&oe=62839DA2')
        c.drawImage(logo, 250, 680.5, width=100, height=100, preserveAspectRatio=True)

        # Datos generales
        c.drawString(30, 650, 'San José, San José 11504')
        c.drawString(30, 630, 'Costa Rica')

        c.drawString(450, 650, 'Servicio al cliente:')
        c.drawString(450, 630, 'tiendatrululucr@gmail.com')

        c.line(30, 590, 585, 590)

        print('En proceso...')

        # Datos generales
        c.drawString(30, 550, f'{today.day}/{today.month}/{today.year} - {today.hour}:{today.minute}.')
        c.drawString(30, 520, f"{apellido}, {nombre}.")
        c.drawString(30, 500, f"{canton}, {provincia}.")
        c.drawString(30, 480, f"{direccion_detallada}")
        c.drawString(30, 460, f"(+506) {telefono}.")
        c.drawString(30, 440, f"{correo}")

        c.drawString(360, 520, 'Enviado a través del Servicio de Encomiendas.')
        c.drawString(443.5, 500, 'Entrega de 1 a 3 días hábiles.')
        c.drawString(442, 480, 'Pago a través de Sinpe Móvil.')

        c.line(30, 400, 585, 400)

        # Datos del pedido

        c.drawString(30, 360, f'Pedido #{id}')
        y = 330

        print('En proceso...')

        for producto in productos:
            if session[producto.nombre] > 0:
                c.drawString(30, y, producto.nombre)
                c.drawString(440, y, str(session[producto.nombre]))
                if(producto.descuento > 0):
                    c.drawString(520, y, f'¢{producto.precio - (producto.precio * (producto.descuento / 100))}')
                else:
                    c.drawString(520, y, f'¢{producto.precio}')
                y -= 30
                if y <= 20:
                    y = 750
                    c.showPage()
                    c.setFont("Times-Roman", 12)

        print('En proceso...')

        c.setFont("Times-Roman", 12)
        y -= 10
        c.line(30, y, 585, y)

        # Total
        y -= 40
        c.drawString(400, y, 'Productos')
        c.drawString(520, y, f'¢{total}')

        y -= 20
        c.drawString(419, y, 'Envío')
        c.drawString(520, y, '¢2.000,00')

        y -= 20
        c.drawString(421, y, 'Total')
        c.drawString(520, y, f'¢{total + 2000}')

        y -= 80

        # Gracias
        c.drawString(245, y, '¡Gracias por preferirnos!')

        # Guardar el PDF
        c.showPage()
        c.save()

        time.sleep(1)

        # Generación del correo
        asunto = 'Pedido realizado exitosamente'
        cuerpo = 'Este es el mensaje'
        ruta_archivo = 'factura.pdf'
        nombre_archivo = 'factura.pdf'

        print('En proceso...')

        mime = MIMEMultipart()
        mime['From'] = 'trululucr@gmail.com'
        mime['To'] = 'edisonthomas1503@gmail.com'
        mime['Subject'] = asunto

        file_format = MIMEText(cuerpo, 'plain')
        mime.attach(file_format)

        archivo_adjunto = open(ruta_archivo, 'rb')

        adjunto_MIME = MIMEBase('application', 'octet-stream')
        adjunto_MIME.set_payload((archivo_adjunto).read())

        encoders.encode_base64(adjunto_MIME)

        adjunto_MIME.add_header('Content-Disposition', "attachment; filename= %s" % nombre_archivo)
        mime.attach(adjunto_MIME)

        session_smtp = smtplib.SMTP('smtp.gmail.com', 587)
        session_smtp.starttls()
        session_smtp.login('trululucr@gmail.com', 'tiendatrululucr020518*-')
        texto = mime.as_string()
        session_smtp.sendmail('trululucr@gmail.com', correo, texto)

        session_smtp.quit()