# Sección de imports de la app
from fileinput import filename
from hashlib import new
from importlib.resources import path
import re
from sys import path_hooks
from flask import *
from flask_wtf import *
from numpy import product
from werkzeug.security import *
from config import *
from models import *
from sqlalchemy import *
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import *
from email.mime.multipart import *
from src.email import Email
from datetime import date, datetime
from werkzeug.utils import secure_filename
from flask_wtf.file import FileField

import mysql.connector
import forms
import math
import smtplib
import time
import os

province = 1
canton = 2
district = 3

# Mantener las cookies activas después de cerrar el navegador
SESSION_PERMANENT = True

# El formato del correo de registro
html_format = """
<center>
    <br style="clear: both;">
    <center>
        <h1 style="font-weight: 400;">¡Ahora <strong>formas parte</strong> de la familia Trululu!</h1>
        <h2 style="font-weight: 400;">Estamos felices de que ahora formes parte de nosotros, por lo que te regalamos un 10% de descuento en tu primera compra.</h2>
    </center>
    <img style="width: 20%;
    float: left;
    margin-left: 40%;"
    src="https://i.ibb.co/8mQN9vd/png.png" alt="Image">
</center>
"""

# Conexión tradicional a la base de datos.
db = mysql.connector.connect(
    host='crtrululudb.cubo7zpmndyq.us-east-1.rds.amazonaws.com',
    username='admin',
    password='Em15032001*',
    database='trululucrdb',
    port='3306'
)

# Convierte el query en un diccionario
cursor = db.cursor(dictionary=True)

# Inicializar proyecto
app = Flask(__name__)

# Establece configuraciones en archivo externo
app.config.from_object(DevelopmentConfig)

# Conexión a la base de datos mediante el ORM SQL Alchemy.
SQLAlchemy(app)

# Proteger la app
csrf = CSRFProtect()

# Se crean variables de sesión con el nombre de cada producto en la BD
def create_products():
    # Invoca a la tabla Producto de la BD
    products = Producto.query.all()

    # Recorrer cada registro de la tabla.
    # Funciona para almacenar la cantidad de cada producto en el carrito del usuario.
    for product in products:
        session[product.nombre] = 0
        # session['aros'] = 3
        # session['gusanos'] = 7

# Encriptamos la contraseña al registrarse
def create_password(password):
    return generate_password_hash(password)

# Comparar la contraseña original con la encriptada al iniciar sesión
def isCorrect(password):
    return check_password_hash(password, request.form['vcontrasena'])

# Obtenemos la contraseña encriptada
def get_password():
    correo = request.form['vcorreo']
    password = sessiondb.query(Usuario.contrasenna).filter(
        Usuario.correo == correo
    )
    return password[0]['contrasenna']

# Obtenemos el usuario conectado
def get_user(correo, contrasena):
    query = 'SELECT * FROM usuario WHERE correo = %s AND contrasenna = %s'
    values = correo, contrasena
    cursor.execute(query, values)
    user = cursor.fetchall()
    return user[0]

@app.route('/register', methods=['GET', 'POST'])
def register():
    # Accedemos a la clase email
    email = Email()
    # Accedemos a los formularios
    register_form = forms.Register(request.form)
    login_form = forms.Login(request.form)
    # Se resetea el diccionario llamado session
    session.clear()

    if request.method == 'POST':
        if(register_form.validate() or login_form.validate()):
            # Cantidad total de productos
            session['products_quantity'] = 0
            # Precio total de los productos
            session['total_price'] = 0
            create_products()
            try:
                if(len(register_form.nombre.data) > 0):
                    nombre = register_form.nombre.data.split()
                    apellido = register_form.apellido.data.split()
                    numeroIdentificacion = register_form.numeroIdentificacion.data
                    correo = register_form.correo.data
                    telefono = register_form.telefono.data
                    username = register_form.username.data
                    contrasena = create_password(register_form.contrasena.data)
                    rol = 1
                    if len(nombre) > 1 and len(apellido) > 1:
                        primer_nombre = nombre[0]
                        segundo_nombre = nombre[1]
                        primer_apellido = apellido[0]
                        segundo_apellido = apellido[1]
                        user = Usuario(nombre1=primer_nombre, nombre2=segundo_nombre, apellido1=primer_apellido,    apellido2=segundo_apellido,  identificacion=numeroIdentificacion, telefono=telefono, correo=correo,    nombre_usuario=username, contrasenna=contrasena, id_rol=rol)
                    elif len(nombre) == 1 and len(apellido) > 1:
                        primer_apellido = apellido[0]
                        segundo_apellido = apellido[1]
                        user = Usuario(nombre1=nombre, apellido1=primer_apellido, apellido2=segundo_apellido,   identificacion=numeroIdentificacion, telefono=telefono, correo=correo, nombre_usuario=username,  contrasenna=contrasena, id_rol=rol)
                    elif len(nombre) > 1 and len(apellido) == 1:
                        primer_nombre = nombre[0]
                        segundo_nombre = nombre[1]
                        user = Usuario(nombre1=primer_nombre, nombre2=segundo_nombre, apellido1=apellido,   identificacion=numeroIdentificacion, telefono=telefono, correo=correo, nombre_usuario=username,  contrasenna=contrasena, id_rol=rol)
                    else:
                        user = Usuario(nombre1=nombre, apellido1=apellido,  identificacion=numeroIdentificacion,    telefono=telefono, correo=correo, nombre_usuario=username, contrasenna=contrasena, id_rol=rol)
                    sessiondb.add(user)
                    sessiondb.commit()
                    success_message = 'Usuario registrado exitosamente, {}'.format(register_form.username.data)
                    flash(success_message)
                    # Email de confirmación
                    # email.send_email([correo], '¡Bienvenido a Tienda Trululu!', html_format, 'html')


            except Exception as ex:
                try:
                    print("Ha habido una excepción", type(ex))
                    sessiondb.rollback()
                    if(len(request.form['vcorreo']) > 0):
                        correo = login_form.vcorreo.data
                        contrasena = get_password()
                        if(isCorrect(contrasena)):
                            query = 'SELECT * FROM usuario WHERE correo = %s AND contrasenna = %s'
                            values = correo, contrasena
                            cursor.execute(query, values)
                            user = cursor.fetchall()
                            print(user)
                            if cursor.rowcount > 0:
                                success_message = 'Ha iniciado sesión exitosamente, {}'.format(user[0].get('nombre1'))
                                flash(success_message)
                                session['user'] = user[0]
                                # Se valida que el usuario tenga direccion
                                if 'user_address' in session:
                                        query = 'SELECT id_direccion FROM usuario where id_usuario = %s'
                                        values = [session['user']['id_usuario'],]
                                        cursor.execute(query, values)

                                        addr = cursor.fetchall()
                                        session['user']['id_direccion'] = addr[0]

                                # Obtener la direccion del usuario
                                if session['user']['id_direccion'] != None:
                                    address = Usuario.query.join(Detalle_Direccion).add_columns(Usuario.id_direccion,   Detalle_Direccion.id_provincia,                       Detalle_Direccion.id_canton,    Detalle_Direccion.id_distrito, Detalle_Direccion.des_direccion).filter(
                                        Usuario.id_usuario == session['user']['id_usuario']
                                    )

                                    for add in address:
                                        province = add.id_provincia
                                        canton = add.id_canton
                                        district = add.id_distrito
                                        session['user_address'] = add.des_direccion
                                        session['user_address_id'] = add.id_direccion

                                    province_query = sessiondb.query(Provincia).filter(
                                        Provincia.id_provincia == province
                                    ).first()

                                    canton_query = sessiondb.query(Canton).filter(
                                        Canton.id_canton == canton
                                    ).first()

                                    district_query = sessiondb.query(Distrito).filter(
                                        Distrito.id_distrito == district
                                    ).first()

                                    session['user_province_name'] = province_query.des_provincia
                                    session['user_canton_name'] = canton_query.des_canton
                                    session['user_district_name'] = district_query.des_distrito

                                    definitive_address = Detalle_Direccion.query.filter(
                                        Detalle_Direccion.id_direccion == session['user']['id_direccion']
                                    )

                                    for df in definitive_address:
                                        session['user_province'] = df.id_provincia

                            return render_template('index.html', user = user[0])
                        else:
                            print(1 + 'a')
                        return redirect(url_for('register'))
                except:
                    success_message = 'Ha ocurrido un error. Verifique sus datos {}'.format('')
                    flash(success_message)
                    print('Lo sentimos, no pudimos encontrar tu cuenta')
        else:
            success_message = 'Lo sentimos, ha ocurrido un error. Verifique los datos. {}'. format('')
            flash(success_message)
    return render_template('register.html', register_form = register_form, login_form = login_form)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/about')
def about():
    return render_template('about-us.html')

@app.route('/profile', methods=['GET', 'POST'])
def profile():
    # global canton
    # global district
    # Se valida que el usuario tenga direccion
    if 'user_address' in session:

        # correo = request.form['vcorreo']
        # password = sessiondb.query(Usuario.contrasenna).filter(
        #     Usuario.correo == correo
        # )
        # return password[0]['contrasenna']

        query_2 = sessiondb.query(Usuario.id_direccion).filter(
            Usuario.id_usuario == session['user']['id_usuario']
        )

        addr = query_2[0]['id_direccion']
        session['user']['id_direccion'] = addr

        # Obtener la direccion del usuario
        if session['user']['id_direccion'] != None:
            address = Usuario.query.join(Detalle_Direccion).add_columns(Usuario.id_direccion,Detalle_Direccion.id_provincia,Detalle_Direccion.id_canton,Detalle_Direccion.id_distrito, Detalle_Direccion.des_direccion).filter(
                Usuario.id_usuario == session['user']['id_usuario']
            )

        global province
        global canton
        global district

        for add in address:
            province = add.id_provincia
            canton = add.id_canton
            district = add.id_distrito
            session['user_address'] = add.des_direccion
            session['user_address_id'] = add.id_direccion

        time.sleep(1)

        province_query = sessiondb.query(Provincia).filter(
            Provincia.id_provincia == province
        ).first()

        canton_query = sessiondb.query(Canton).filter(
            Canton.id_canton == canton
        ).first()

        district_query = sessiondb.query(Distrito).filter(
            Distrito.id_distrito == district
        ).first()

        print(province)
        print(canton)
        print(district)

        session['user_province_name'] = province_query.des_provincia
        session['user_canton_name'] = canton_query.des_canton
        session['user_district_name'] = district_query.des_distrito
    update_form = forms.UpdateUser(request.form)
    if request.method == 'POST' and (update_form.validate()):

        user = sessiondb.query(Usuario).filter(
            Usuario.id_usuario == session['user']['id_usuario']
        ).first()

        # Obtenemos el nombre y el apellido para validar si son uno o dos
        nombre = update_form.nombre.data.split()
        apellido = update_form.apellido.data.split()
        if len(nombre) > 1 and len(apellido) > 1:
            user.nombre1 = nombre[0]
            user.nombre2 = nombre[1]
            user.apellido1 = apellido[0]
            user.apellido2 = apellido[1]
        elif len(nombre) == 1 and len(apellido) > 1:
            user.nombre1 = nombre
            user.nombre2 = None
            user.apellido1 = apellido[0]
            user.apellido2 = apellido[1]
        elif len(nombre) > 1 and len(apellido) == 1:
            user.nombre1 = nombre[0]
            user.nombre2 = nombre[1]
            user.apellido1 = apellido
            user.apellido2 = None
        else:
            user.nombre1 = nombre
            user.nombre2 = None
            user.apellido1 = apellido
            user.apellido2 = None

        # Actualizamos los datos del usuario
        user.identificacion = update_form.numeroIdentificacion.data
        user.correo = update_form.correo.data
        user.telefono = update_form.telefono.data
        user.nombre_usuario = update_form.username.data

        # Añadimos los cambios y los guardamos
        sessiondb.add(user)
        sessiondb.commit()

        success_message = 'Usuario actualizado con éxito. {}'. format('')
        flash(success_message)

        session['user'] = {
            'id_usuario': user.id_usuario,
            'nombre1': user.nombre1,
            'nombre2': user.nombre2,
            'apellido1': user.apellido1,
            'apellido2': user.apellido2,
            'identificacion': user.identificacion,
            'telefono': user.telefono,
            'correo': user.correo,
            'contrasenna': user.contrasenna,
            'nombre_usuario': user.nombre_usuario,
            'id_direccion': user.id_direccion,
            'id_rol': user.id_rol
        }
    return render_template('profile.html', update_form = update_form)

@app.route('/update-password', methods=['GET', 'POST'])
def update_password():
    password_form = forms.UpdatePassword(request.form)
    if request.method == 'POST' and (password_form.validate()):
        user = sessiondb.query(Usuario).filter(
            Usuario.id_usuario == session['user']['id_usuario']
        ).first()
        user.contrasenna = create_password(password_form.contrasena.data)
        session['user']['contrasenna'] = user.contrasenna
        sessiondb.add(user)
        sessiondb.commit()
        success_message = 'Contraseña actualizada exitosamente. {}'. format('')
        flash(success_message)

    return render_template('update-password.html', password_form = password_form)

@app.route('/add-province', methods=['GET', 'POST'])
def add_province():
    provinces = Provincia.query.all()
    if request.method == 'POST':
        session['user_province'] = request.form['province']
        return redirect(url_for('add_canton'))
    return render_template('add-province.html', provinces = provinces)

@app.route('/add-canton', methods=['GET', 'POST'])
def add_canton():
    cantons = sessiondb.query(Canton).filter(
        Canton.id_provincia == session['user_province']
    )
    province_name = sessiondb.query(Provincia.des_provincia).filter(
        Provincia.id_provincia == session['user_province']
    )

    for pvn in province_name:
        prov = pvn

    prov = str(prov)

    characters = "(',)"

    for x in range(len(characters)):
        prov = prov.replace(characters[x],"")

    session['user_province_name'] = prov

    if request.method == 'POST':
        session['user_canton'] = request.form['canton']
        return redirect(url_for('add_distrito'))
    return render_template('add-canton.html', cantons = cantons, province_name = prov)

@app.route('/add-distrito', methods=['GET', 'POST'])
def add_distrito():
    canton_name = sessiondb.query(Canton.des_canton).filter(
        Canton.id_canton == session['user_canton']
    )
    for ctn in canton_name:
        cant = ctn

    cant = str(cant)

    characters = "(',)"

    for x in range(len(characters)):
        cant = cant.replace(characters[x],"")

    session['user_canton_name'] = cant
    districts = sessiondb.query(Distrito).filter(
        Distrito.id_canton == session['user_canton']
    )
    if request.method == 'POST':
        session['user_district'] = request.form['district']
        return redirect(url_for('add_direccion'))
    return render_template('add-district.html', districts = districts, canton_name = cant)

@app.route('/add-direccion', methods=['GET', 'POST'])
def add_direccion():
    district_name = sessiondb.query(Distrito.des_distrito).filter(
        Distrito.id_distrito == session['user_district']
    )
    for dst in district_name:
        distr = dst

    distr = str(distr)

    characters = "(',)"

    for x in range(len(characters)):
        distr = distr.replace(characters[x],"")

    session['user_district_name'] = distr

    if request.method == 'POST':
        if 'user_address' in session:
            session['updating'] = True

        session['user_address'] = request.form['address']
        return redirect(url_for('confirmar_direccion'))
    return render_template('add-address.html', district_name = distr)

@app.route('/confirm-data', methods=['GET', 'POST'])
def confirmar_direccion():
    if request.method == 'POST':
        if 'updating' in session:
            address = Usuario.query.join(Detalle_Direccion).add_columns(Usuario.id_direccion, Detalle_Direccion.id_provincia, Detalle_Direccion.id_canton, Detalle_Direccion.id_distrito, Detalle_Direccion.des_direccion).filter(
                Usuario.id_usuario == session['user']['id_usuario']
            )
            definitive_address = Detalle_Direccion.query.filter(
                Detalle_Direccion.id_direccion == session['user']['id_direccion']
            )
            definitive_address.id_provincia = session['user_province']
            definitive_address.id_canton = session['user_canton']
            definitive_address.id_distrito = session['user_district']
            definitive_address.des_direccion = session['user_address']

            query = 'UPDATE detalle_direccion SET des_direccion = %s, id_provincia = %s, id_canton = %s, id_distrito = %s WHERE id_direccion = %s'
            values = session['user_address'], session['user_province'], session['user_canton'], session['user_district'], session['user']['id_direccion']

            print(type(query))
            print(type(values))

            cursor.execute(query, values)
            db.commit()

            return redirect(url_for('home'))
        else:
            address = Detalle_Direccion(des_direccion=session['user_address'], id_provincia=session['user_province'], id_canton=session['user_canton'], id_distrito=session['user_district'])
            sessiondb.add(address)
            sessiondb.commit()

            user = sessiondb.query(Usuario).filter(
                Usuario.id_usuario == session['user']['id_usuario']
            ).first()

            user.id_direccion = address.id_direccion
            sessiondb.add(user)
            sessiondb.commit()

            return redirect(url_for('home'))

    return render_template('confirm-data.html')

@app.route('/quotation', methods=['GET', 'POST'])
def quotation():
    if request.method == 'POST':
        id = session['user']['id_usuario']
        quot = request.form['quotation']
        query = Cotizacion(des_cotizacion=quot, id_usuario=id)
        sessiondb.add(query)
        sessiondb.commit()
        return redirect(url_for('my_quotation'))
    return render_template('quotation.html')

@app.route('/my-quotation', methods=['GET', 'POST'])
def my_quotation():
    quots = Cotizacion.query.join(Usuario).add_columns(
        Cotizacion.id_cotizacion, Cotizacion.id_usuario, Cotizacion.des_cotizacion, Cotizacion.respuesta, Usuario.nombre1, Usuario.apellido1, Usuario.id_usuario
    ).filter(
        Cotizacion.id_usuario == session['user']['id_usuario']
    ).all()
    return render_template('my_quotation.html', quots=quots)

@app.route('/quotation-adm', methods=['GET', 'POST'])
def quotation_adm():
    quots = Cotizacion.query.join(Usuario).add_columns(
        Cotizacion.id_cotizacion, Cotizacion.id_usuario, Cotizacion.des_cotizacion, Cotizacion.respuesta, Usuario.nombre1, Usuario.apellido1, Usuario.id_usuario
    ).all()
    print(quots)
    return render_template('quotation_adm.html', quots=quots)

@app.route('/quotation-ans/<id_cotizacion>', methods=['GET', 'POST'])
def quotation_ans(id_cotizacion):

    query=Cotizacion.query.filter( Cotizacion.id_cotizacion == id_cotizacion).first()

    usuario=query.id_usuario

    user = Usuario.query.filter(
        Usuario.id_usuario == usuario
    ).first()

    if request.method == 'POST':
        cotizacion= sessiondb.query(Cotizacion).filter(Cotizacion.id_cotizacion == id_cotizacion).first()

        if cotizacion.respuesta != request.form['answer']:
            cotizacion.respuesta = request.form['answer']

        sessiondb.add(cotizacion)
        sessiondb.commit()

        return redirect(url_for('quotation_adm'))
    return render_template('quotation_ans.html', usuario=user)

@app.route('/rol', methods=['GET', 'POST'])
def rol():
    users = Usuario.query.join(Rol).add_columns(
        Usuario.id_usuario, Usuario.nombre1, Usuario.apellido1, Rol.id_rol, Rol.des_rol
    ).filter(
        Usuario.id_usuario != session['user']['id_usuario']
    ).all()

    roles = Rol.query.all()

    if request.method == 'POST':
        for user in users:
            usuario= sessiondb.query(Usuario).filter(Usuario.id_usuario == user.id_usuario ).first()

            if request.form[str(user.id_usuario)] == '1':
                if usuario.id_rol != request.form[str(user.id_usuario)]:
                    usuario.id_rol=1
            elif request.form[str(user.id_usuario)] == '2':
                if usuario.id_rol != request.form[str(user.id_usuario)]:
                    usuario.id_rol=2
            else:
                if usuario.id_rol != request.form[str(user.id_usuario)]:
                    usuario.id_rol=3

            sessiondb.add(usuario)
            sessiondb.commit()

        #registro de bitacora de acciones
        nombre_accion= f'Se cambio el rol del usuario por el id del rol {rol}'
        origen="Gestion de roles"
        id_usuario=session['user']['id_usuario']

        query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
        sessiondb.add(query)
        sessiondb.commit()

        return redirect(url_for('rol'))
    return render_template('rol.html', users=users, roles=roles)

@app.route('/report')
@app.route('/report/<int:page>')
def report(page = 1):
    per_page = 9
    bitacoras = Bitacora.query.paginate(page, per_page, False)
    ventas = Ventas.query.join(Producto).add_columns(
        Producto.nombre, Ventas.ventas_mes, Ventas.fecha).filter(
        extract('month',Ventas.fecha)== extract('month',datetime.now()))

    for counter in range(math.ceil(15/9)):
        if(page == counter + 1):
            session['selected'] = counter + 1
            break

    return render_template('report.html',bitacoras=bitacoras, ventas=ventas, pages = math.ceil(15/per_page))

@app.route('/inventory', methods=['GET', 'POST'])
def inventory():
    productos = Producto.query.join(Marca).add_columns(Producto.id_producto, Producto.cantidad, Producto.nombre, Producto.descuento, Producto.precio, Marca.des_marca, Producto.imagen)
    marcas = Marca.query.all()

    session['productos'] = dir(productos)
    session['marcas'] = marcas

    if request.method == 'POST':
        if request.form['funcion'] == 'agregar':
            return redirect(url_for('register_product'))
        elif request.form['funcion']!='agregar':
            session['product_edit'] = request.form['funcion']
            return redirect(url_for('edit_product'))
    return render_template('inventory.html', marcas = marcas, productos = productos)

app.config["UPLOAD_FOLDER"] = "./static/uploads"

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

def allowed_file(file):
    file = file.split('.')
    if file[1] in ALLOWED_EXTENSIONS:
        return True
    return False

@app.route('/register-product',methods=['GET', 'POST'])
def register_product():
    marcas= Marca.query.all()

    if request.method == 'POST':
        nombre=request.form['product_name']
        descripcion=request.form['product_desc']
        cantidad=request.form['product_count']
        stock=request.form['product_stock']
        precio=float(request.form['product_precio'])
        imagen=request.files['product_image']
        descuento=float(request.form['product_descuento'])
        disponible=request.form['product_disponible']
        id_marca=request.form['marca']
        iva=float(precio*0.13)

        filename = secure_filename(imagen.filename)

        if imagen and allowed_file(filename):
            imagen.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))

        query = Producto(nombre=nombre, descripcion =descripcion,imagen=imagen,
        cantidad=cantidad,stock=stock,precio=precio,descuento=descuento,iva=iva,disponible=disponible,id_marca=id_marca)
        sessiondb.add(query)
        sessiondb.commit()


        #registro de bitacora de acciones
        nombre_accion=f'Se ingreso un producto en el inventario con el nombre de {nombre}'
        origen="inventario"
        id_usuario=session['user']['id_usuario']

        query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
        sessiondb.add(query)
        sessiondb.commit()

        return redirect(url_for('inventory'))

    return render_template('register-product.html',marcas=marcas)

@app.route('/edit-product',methods=['GET', 'POST'])
def edit_product():
    productos=Producto.query.filter(Producto.id_producto == session['product_edit']).all()
    marcas = Marca.query.all()

    if request.method == 'POST':
        if request.form['funcion'] == 'editado':
            producto = sessiondb.query(Producto).filter(Producto.id_producto == session['product_edit']).first()
            if producto.nombre != request.form['product_name']:
                producto.nombre = request.form['product_name']

            if producto.descripcion != request.form['product_desc']:
                producto.descripcion = request.form['product_desc']
            if producto.cantidad != request.form['product_count']:
                producto.cantidad = request.form['product_count']
            if producto.precio != request.form['product_precio']:
                producto.precio = float(request.form['product_precio'])
                iva=float(producto.precio*0.13)
                iva=iva
            if producto.stock != request.form['product_stock']:
                producto.stock = request.form['product_stock']
            if producto.descuento != request.form['product_descuento']:
                producto.descuento = request.form['product_descuento']
            if producto.imagen != request.files['product_image']:
                producto.imagen = request.files['product_image']
            if producto.disponible != request.form['product_disponible']:
                producto.disponible = request.form['product_disponible']
            if producto.id_marca != request.form['marca']:
                producto.id_marca = request.form['marca']
            else:
                print("no se pudo registrar")

            sessiondb.add(producto)
            sessiondb.commit()

             #registro de bitacora de acciones
            nombre_accion= f'Se actualizo un producto en el inventario con el nombre de {producto.nombre}'
            origen="inventario"
            id_usuario=session['user']['id_usuario']

            query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
            sessiondb.add(query)
            sessiondb.commit()

            return redirect(url_for('inventory'))

        elif request.form['funcion']!='editar':
            id=request.form['funcion']

            sessiondb.query(Producto).filter(
            Producto.id_producto==id
            ).delete()
            sessiondb.commit()

            #registro de bitacora de acciones
            nombre_accion= f'Se elimino un producto en el inventario con el id {id}'
            origen="inventario"
            id_usuario=session['user']['id_usuario']

            query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
            sessiondb.add(query)
            sessiondb.commit()


            return redirect(url_for('inventory'))

    return render_template('edit-product.html',productos=productos , marcas=marcas)

@app.route('/events' ,methods=['GET', 'POST'])
def events():
    sucursales = Sucursal.query.all()
    if request.method == 'POST':
        print('-------------------------------------')
        print(os.getcwd())
        #se llaman los datos de eventos
        idSucursal = request.form['sucursal']
        event_title = request.form['event_title']
        event_desc=request.form['event_desc']
        event_horario=request.form['event_horario']
        event_image="No tiene una imagen asignada"

        #se registra el evento

        query = Evento(nombre=event_title, descripcion =event_desc,imagen=event_image, fecha=datetime.today(),horario=event_horario)
        sessiondb.add(query)
        sessiondb.commit()

        querysucursal=Sucursal_Evento(id_sucursal=idSucursal ,id_evento=query.id_evento)
        sessiondb.add(querysucursal)
        sessiondb.commit()

        #registro de bitacora de acciones
        nombre_accion= f'Se ingreso un evento con el nombre de {event_title}'
        origen="Evento"
        id_usuario=session['user']['id_usuario']

        query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
        sessiondb.add(query)
        sessiondb.commit()

    return render_template('events.html',sucursales = sucursales)

@app.route('/events_list' ,methods=['GET', 'POST'])
def events_list():
    eventos=Evento.query.all()

    if request.method == 'POST':
        id=request.form['eliminado']

        sessiondb.query(Sucursal_Evento).filter(
        Sucursal_Evento.id_evento==id
        ).delete()
        sessiondb.commit()

        sessiondb.query(Evento).filter(
        Evento.id_evento==id
        ).delete()
        sessiondb.commit()

        #registro de bitacora de acciones
        nombre_accion= f'Se elimino un evento con el id {id}'
        origen="Evento"
        id_usuario=session['user']['id_usuario']

        query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
        sessiondb.add(query)
        sessiondb.commit()

        return redirect(url_for('events_list'))

    eventos=Evento.query.all()

    return render_template('events_list.html',eventos=eventos)

@app.route('/shop', methods=['GET', 'POST'])
@app.route('/shop/<int:page>', methods=['GET', 'POST'])
def shop(page = 1):
    per_page = 9
    session['total_products_quan'] = 0
    products = Producto.query.join(Marca).add_columns(Producto.id_producto, Producto.cantidad, Producto.nombre, Producto.descuento, Producto.precio, Marca.des_marca, Producto.imagen).paginate(page, per_page, False)
    brands = Marca.query.all()

    query = 'SELECT * FROM producto WHERE id_producto > %s'
    values = 0,
    cursor.execute(query, values)
    prod = cursor.fetchall()
    for p in prod:
        print(p)

    session['products'] = dir(products)
    session['brands'] = brands

    # session['total_products_quan'] = products.

    if request.method == 'POST':
        session['products_quantity'] += 1
        id = request.form['product_id']
        product = sessiondb.query(Producto).filter(
            Producto.id_producto == id
        )
        for pdt in product:
            session[pdt.nombre] += 1
            if(pdt.descuento != 0):
                session['total_price'] += pdt.precio - (pdt.precio * (pdt.descuento / 100))
            else:
                session['total_price'] += pdt.precio

    for counter in range(math.ceil(15/9)):
        if(page == counter + 1):
            session['selected'] = counter + 1
            break

    return render_template('shop.html', products=products, brands=brands, pages = math.ceil(15/per_page))

@app.route('/product-details')
def product_details():
    return render_template('product-details.html')

@app.route('/cart-page', methods=['GET', 'POST'])
def cart_page():
    products = Producto.query.all()
    if request.method == 'POST':
        if request.form['product_name'] == '0':
            session['products_quantity'] = 0
            session['total_price'] = 0
            product = sessiondb.query(Producto).all()

            for pdt in product:
                session[pdt.nombre] = 0
        else:
            quan = session[request.form['product_name']]
            session[request.form['product_name']] = 0
            session['products_quantity'] -= quan
            session['total_price'] -= float(request.form['product_value']) * float(quan)
    return render_template('cart-page.html', products=products)

@app.route('/payment', methods=['GET', 'POST'])
def payment():
    stores = Sucursal.query.all()
    if request.method == 'POST':
        if session['user']['id_direccion'] != None:
            address = Usuario.query.join(Detalle_Direccion).add_columns(Usuario.id_direccion, Detalle_Direccion.id_provincia, Detalle_Direccion.id_canton, Detalle_Direccion.id_distrito, Detalle_Direccion.des_direccion).filter(
                Usuario.id_usuario == session['user']['id_usuario']
            )

            global province
            global canton
            global district

            for add in address:
                province = add.id_provincia
                canton = add.id_canton
                district = add.id_distrito
                session['user_address'] = add.des_direccion
                session['user_address_id'] = add.id_direccion

            province_query = sessiondb.query(Provincia).filter(
                Provincia.id_provincia == province
            ).first()

            canton_query = sessiondb.query(Canton).filter(
                Canton.id_canton == canton
            ).first()

            district_query = sessiondb.query(Distrito).filter(
                Distrito.id_distrito == district
            ).first()

            session['user_province_name'] = province_query.des_provincia
            session['user_canton_name'] = canton_query.des_canton
            session['user_district_name'] = district_query.des_distrito
        email = Email()
        products = Producto.query.all()

        # Se agrega la factura
        telefono=session['user']['telefono']
        total=session['products_quantity']
        precio=session['total_price']
        usuario=session['user']['id_usuario']

        query=Usuario.query.filter(Usuario.id_usuario==usuario).first()

        session['user']['id_direccion']=query.id_direccion

        facturar = Factura(titulo='Tienda Trululu CR', fecha=datetime.now(), telefono=telefono, iva=0.13,
        total_productos=total, total_precio=precio, id_metodo=3,
        id_direccion= session['user']['id_direccion'], id_usuario=usuario)
        sessiondb.add(facturar)
        sessiondb.commit()

        # Se agrega el pedido
        order = Pedido(estado='En proceso', id_factura=facturar.id_factura, id_usuario=session['user']['id_usuario'], id_provincia=session['user_province'])
        sessiondb.add(order)
        sessiondb.commit()

        # Se agregan los productos a la factura
        product = sessiondb.query(Producto).all()

        for pdt in product:
            if session[pdt.nombre] > 0:
                product_invoice = Producto_Factura(id_producto=pdt.id_producto, cantidad=session[pdt.nombre], id_factura=facturar.id_factura)
                sessiondb.add(product_invoice)
                sessiondb.commit()

        time.sleep(1)

        #email.genera_factura(facturar.id_factura, session['user']['nombre1'], session['user']['apellido1'], session['user_province_name'], session['user_canton_name'], session['user_address'], session['user']['telefono'], session['user']['correo'], products, session['total_price'])

        session['products_quantity'] = 0
        session['total_price'] = 0
        product = sessiondb.query(Producto).all()

        create_products()

        #for pdt in product:

            #session[pdt.nombre] = 0

        #se ingresa los datos al informe de ventas

        #venta = sessiondb.query(Ventas).filter(Ventas.id_producto == ).first()

        return redirect(url_for('home'))
    return render_template('payment.html', stores = stores)

@app.route('/receive/<id_factura>')
def receive(id_factura):
    query=Factura.query.filter(Factura.id_factura == id_factura).first()
    customer_id = query.id_usuario

    user = Usuario.query.filter(
        Usuario.id_usuario == customer_id
    ).first()

    invoice = Producto_Factura.query.join(Producto).add_columns(Producto_Factura.id_factura, Producto_Factura.cantidad, Producto.nombre, Producto.precio, Producto.descuento).filter(
        Producto_Factura.id_factura == id_factura
    ).all()

    return render_template('receive.html', usuario=user, fecha=query.fecha, factura=invoice, total=query.total_precio)

@app.route('/receive_menu')
def receive_menu():
    query=Factura.query.filter(Factura.id_usuario == session['user']['id_usuario']).all()

    return render_template('receive_menu.html', facturas=query)

@app.route('/order')
def order():
    orders = sessiondb.query(Pedido).filter(
        Pedido.id_usuario == session['user']['id_usuario']
    )
    return render_template('order.html', orders=orders)

@app.route('/order-adm', methods=['GET', 'POST'])
def order_adm():
    orders = Pedido.query.join(Usuario).join(Provincia).add_columns(
        Pedido.id_pedido, Usuario.nombre1, Usuario.apellido1, Usuario.id_usuario, Pedido.estado, Pedido.id_provincia, Pedido.id_factura, Provincia.des_provincia).all()

    if request.method == 'POST':

        for order in orders:
            pedido = sessiondb.query(Pedido).filter(
                Pedido.id_pedido == order.id_pedido
            ).first()

            if request.form[str(order.id_pedido)] == 'processing':
                pedido.estado = 'En proceso'
            elif request.form[str(order.id_pedido)] == 'shipped':
                pedido.estado = 'Enviado'
            else:
                pedido.estado = 'Entregado'

            sessiondb.add(pedido)
            sessiondb.commit()

        #registro de bitacora de acciones
        nombre_accion= f'Se cambio el estado del pedido {order.id_pedido}'
        origen="Envios"
        id_usuario=session['user']['id_usuario']

        query=Bitacora(nombre_accion=nombre_accion,origen=origen,fecha=datetime.now(),id_usuario=id_usuario)
        sessiondb.add(query)
        sessiondb.commit()
        return redirect(url_for('order_adm'))

    orders = Pedido.query.join(Usuario).join(Provincia).add_columns(
    Pedido.id_pedido, Usuario.nombre1, Usuario.apellido1, Usuario.id_usuario, Pedido.estado, Pedido.id_provincia, Pedido.id_factura, Provincia.des_provincia).all()

    return render_template('order_adm.html', orders=orders)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.html'), 404

if __name__ == '__main__':
    db2.init_app(app)
    base.metadata.create_all(engine)
    # mail.init_app(app)

    app.run()
else:
    base.metadata.create_all(engine)