from wtforms import *
from wtforms.fields import *

class Register(Form):
    nombre = StringField('name',
    [
        validators.length(min=3, message='El nombre debe contener al menos 3 caracteres'),
        validators.InputRequired(message = '* Requerido')
        # validators.Regexp('^([a-zA-Z]{2,}\s*[a-zA-Z]{1,})$', message='El nombre no debe contener números')
    ],
    render_kw={"placeholder": "Nombre"})

    apellido = StringField('surname',
    [
        validators.length(min=2, message='El apellido debe contener al menos 2 caracteres'),
        validators.InputRequired(message = '* Requerido')
        # validators.Regexp('^([a-zA-Z]{2,}\s*[a-zA-Z]{1,})$', message='El apellido no debe contener números')
    ],
    render_kw={"placeholder": "Apellidos"})

    numeroIdentificacion = StringField('id_number',
    [
        validators.length(min=8, message='La identificación debe contener al menos 8 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Número de identificación"})

    correo = EmailField('Correo electrónico',
    [
        validators.length(min=12, message='El correo electrónico debe contener al menos 12 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Correo electrónico"})

    telefono = StringField('phone',
    [
        validators.length(min=8, message='Su número telefónico debe contener al menos 8 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Número telefónico"})

    username = StringField('Nombre de usuario',
    [
        validators.length(min=6, message='Su nombre de usuario debe contener al menos 6 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Nombre de usuario"})

    contrasena = PasswordField("password",
    [
        validators.length(min=8, message='La contraseña debe contener al menos 8 caracteres'),
        validators.InputRequired(message = '* Requerido')
        # validators.Regexp('^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$', message='La contraseña debe contener al menos una letra, un número y un caracter especial')
    ],
    render_kw={"placeholder": "Contraseña"})

    btnRegistrar = SubmitField("Registrarme")

class Login(Form):
    vcorreo = EmailField('Correo electrónico',
    [
        validators.length(min=12, message='El correo electrónico no existe'),
        validators.InputRequired(message='* Requerido')
    ],
    render_kw={"placeholder": "Correo electrónico"})

    vcontrasena = PasswordField("password",
    [
        validators.length(min=8, message='La contraseña no existe'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Contraseña"})

class AddToCart(Form):
    product_id = HiddenField('ID')
    btnRegistrar = SubmitField("+ Añadir al carrito")

class UpdateUser(Form):
    nombre = StringField('name',
    [
        validators.length(min=3, message='El nombre debe contener al menos 3 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Nombre"})

    apellido = StringField('surname',
    [
        validators.length(min=2, message='El apellido debe contener al menos 2 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Apellidos"})

    numeroIdentificacion = StringField('id_number',
    [
        validators.length(min=8, message='La identificación debe contener al menos 8 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Número de identificación"})

    correo = EmailField('Correo electrónico',
    [
        validators.length(min=12, message='El correo electrónico debe contener al menos 12 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Correo electrónico"})

    telefono = StringField('phone',
    [
        validators.length(min=8, message='Su número telefónico debe contener al menos 8 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Número telefónico"})

    username = StringField('Nombre de usuario',
    [
        validators.length(min=6, message='Su nombre de usuario debe contener al menos 6 caracteres'),
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Nombre de usuario"})

    btnActualizar = SubmitField("Actualizar datos")

class UpdatePassword(Form):
    contrasena = PasswordField("password",
    [
        validators.length(min=8, message='La contraseña debe contener al menos 8 caracteres'),
    ],
    render_kw={"placeholder": "Nueva contraseña"})

    confirmar_contrasena = PasswordField("password",
    [
        validators.EqualTo('contrasena', "Las contraseñas no coinciden")
    ],
    render_kw={"placeholder": "Confirmar contraseña"})

class Direccion(Form):
    des_direccion = StringField("Dirección detallada",
    [
        validators.InputRequired(message = '* Requerido')
    ],
    render_kw={"placeholder": "Dirección detallada"})

