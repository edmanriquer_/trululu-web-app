![](https://i.ibb.co/6RxS2Qc/IWVgtzkd-4x-removebg-preview.png)
# Nombre del proyecto
Trululu Web App
## Integrantes
- Edison Alejandro Manrique Miranda
- Eduardo Isaac Acuña Porras
- Esteban Vargas Picado
- Johnny Josué Mata Retana
## Descripción del proyecto
Tienda Trululu es una empresa dedicada a la producción y distribución de los más ricos dulces a nivel latinoamericano. En esta ocasión, necesitan apoyo tecnológico con el objetivo de mejor su sitio web y convertirlo en una aplicación web.
## Cómo instalar el repositorio en el equipo para desarrollo
### Paso 1: Ingresar al repositorio en BitBucket y copiar el siguiente comando:
`git clone https://bitbucket.org/edmanriquer_/trululu-web-app.git `
### Paso 2: Seleccionar la ubicación en el almacenamiento de la computadora en donde  desee guardar el proyecto
### Paso 3: Abrir Git Bash y pegar el comando anteriomente copiado en el Paso 1
