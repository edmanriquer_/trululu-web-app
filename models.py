from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import *
from sqlalchemy.orm import *
from config import *
from flask_sqlalchemy import SQLAlchemy
from dataclasses import dataclass
from flask_marshmallow import Marshmallow

engine = create_engine(DevelopmentConfig.SQLALCHEMY_DATABASE_URI, pool_size=20, max_overflow=0, pool_recycle=3600, pool_pre_ping=True)
base = declarative_base()

db2 = SQLAlchemy()


@dataclass
class Provincia(base, db2.Model):
    __tablename__ = 'provincia'

    id_provincia = db2.Column(db2.Integer, primary_key=True)
    des_provincia = db2.Column(db2.String(50), nullable=False)

@dataclass
class Canton(base, db2.Model):
    __tablename__ = 'canton'

    id_canton = db2.Column(db2.Integer, primary_key=True)
    des_canton = db2.Column(db2.String(50), nullable=False) 
    id_provincia = db2.Column(db2.Integer, ForeignKey('provincia.id_provincia'), nullable=False)

@dataclass
class Distrito(base, db2.Model):
    __tablename__ = 'distrito'

    id_distrito = db2.Column(db2.Integer, primary_key=True)
    des_distrito = db2.Column(db2.String(50), nullable=False)
    id_canton = db2.Column(db2.Integer, ForeignKey('canton.id_canton'), nullable=False)

@dataclass
class Detalle_Direccion(base, db2.Model):
    __tablename__ = 'detalle_direccion'

    id_direccion = db2.Column(db2.Integer, primary_key=True)
    des_direccion = db2.Column(db2.Text, nullable=False)
    id_provincia = db2.Column(db2.Integer, ForeignKey('provincia.id_provincia'), nullable=False)
    id_canton = db2.Column(db2.Integer, ForeignKey('canton.id_canton'), nullable=False)
    id_distrito = db2.Column(db2.Integer, ForeignKey('distrito.id_distrito'), nullable=False)

@dataclass
class Sucursal(base, db2.Model):
    __tablename__ = 'sucursal'

    id_sucursal = db2.Column(db2.Integer, primary_key=True)
    nombre = db2.Column(db2.String(30), nullable=False)
    horario = db2.Column(db2.String(30), nullable=False)
    imagen = db2.Column(db2.Text, nullable=False)
    id_direccion = db2.Column(db2.Integer, ForeignKey('detalle_direccion.id_direccion'), nullable=False)

@dataclass
class Evento(base, db2.Model):
    __tablename__ = 'evento'

    id_evento = db2.Column(db2.Integer, primary_key=True)
    horario = db2.Column(db2.String(30), nullable=False)
    nombre = db2.Column(db2.String(100), nullable=False)
    descripcion = db2.Column(db2.Text, nullable=False)
    imagen = db2.Column(db2.Text, nullable=False)
    fecha = db2.Column(db2.Date, nullable=False)

@dataclass
class Sucursal_Evento(base, db2.Model):
    __tablename__ = 'sucursal_evento'

    id_sucursal_evento = db2.Column(db2.Integer, primary_key=True)
    id_sucursal = db2.Column(db2.Integer, ForeignKey('sucursal.id_sucursal'), nullable=False)
    id_evento = db2.Column(db2.Integer, ForeignKey('evento.id_evento'), nullable=False)

@dataclass
class Metodo(base, db2.Model):
    __tablename__ = 'metodo'

    id_metodo = db2.Column(db2.Integer, primary_key=True)
    des_metodo = db2.Column(db2.String(30), nullable=False)

@dataclass
class Rol(base, db2.Model):
    __tablename__ = 'rol'

    id_rol = db2.Column(db2.Integer, primary_key=True)
    des_rol = db2.Column(db2.String(30), nullable=False)

@dataclass
class Usuario(base, db2.Model):
    __tablename__ = 'usuario'

    id_usuario = db2.Column(db2.Integer, primary_key=True)
    nombre1 = db2.Column(db2.String(30), nullable=False)
    nombre2 = db2.Column(db2.String(30), nullable=True)
    apellido1 = db2.Column(db2.String(30), nullable=False)
    apellido2 = db2.Column(db2.String(30), nullable=True)
    identificacion = db2.Column(db2.String(30), nullable=False, unique=True)
    telefono = db2.Column(db2.String(30), nullable=False)
    correo = db2.Column(db2.String(100), nullable=False, unique=True)
    nombre_usuario = db2.Column(db2.String(50), nullable=False, unique=True)
    contrasenna = db2.Column(db2.Text, nullable=False)
    id_rol = db2.Column(db2.Integer, ForeignKey('rol.id_rol'), nullable=False)
    id_direccion = db2.Column(db2.Integer, ForeignKey('detalle_direccion.id_direccion'), nullable=True)

@dataclass
class Marca(base, db2.Model):

    __tablename__ = 'marca'
    id_marca = db2.Column(db2.Integer, primary_key=True)
    des_marca = db2.Column(db2.String(50), nullable=False)

@dataclass
class Producto(base, db2.Model):

    __tablename__ = 'producto'
    id_producto = db2.Column(db2.Integer, primary_key=True)
    nombre = db2.Column(db2.String(50), nullable=False)
    stock = db2.Column(db2.Integer, nullable=False)
    descripcion = db2.Column(db2.Text, nullable=False)
    cantidad = db2.Column(db2.String(20), nullable=False)
    precio = db2.Column(db2.Float, nullable=False)
    iva = db2.Column(db2.Float, nullable=False)
    descuento = db2.Column(db2.Float, nullable=False)
    imagen = db2.Column(db2.Text, nullable=False)
    disponible = db2.Column(db2.String(1), nullable=False)
    id_marca = db2.Column(db2.Integer, ForeignKey('marca.id_marca'), nullable=False)

@dataclass
class Factura(base, db2.Model):

    __tablename__ = 'factura'
    id_factura = db2.Column(db2.Integer, primary_key=True)
    titulo = db2.Column(db2.String(100), nullable=False)
    fecha = db2.Column(db2.Date, nullable=False)
    telefono = db2.Column(db2.String(30), nullable=False)
    iva = db2.Column(db2.Float, nullable=False)
    total_productos = db2.Column(db2.Integer, nullable=False)
    total_precio = db2.Column(db2.Float, nullable=False)
    id_metodo = db2.Column(db2.Integer, ForeignKey('metodo.id_metodo'), nullable=False)
    id_direccion = db2.Column(db2.Integer, ForeignKey('detalle_direccion.id_direccion'), nullable=False)
    id_usuario = db2.Column(db2.Integer, ForeignKey('usuario.id_usuario'), nullable=False)

@dataclass
class Producto_Factura(base, db2.Model):

    __tablename__ = 'producto_factura'
    id_producto_factura = db2.Column(db2.Integer, primary_key=True)
    id_producto = db2.Column(db2.Integer, ForeignKey('producto.id_producto'), nullable=False)
    cantidad = db2.Column(db2.Integer, nullable=False)
    id_factura = db2.Column(db2.Integer, ForeignKey('factura.id_factura'), nullable=False)

@dataclass
class Pedido(base, db2.Model):

    __tablename__ = 'pedido'
    id_pedido = db2.Column(db2.Integer, primary_key=True)
    estado = db2.Column(db2.String(50), nullable=False)
    id_factura = db2.Column(db2.Integer, ForeignKey('factura.id_factura'), nullable=False)
    id_usuario = db2.Column(db2.Integer, ForeignKey('usuario.id_usuario'), nullable=False)
    id_provincia = db2.Column(db2.Integer, ForeignKey('provincia.id_provincia'), nullable=False)

@dataclass
class Cotizacion(base, db2.Model):

    __tablename__ = 'cotizacion'
    id_cotizacion = db2.Column(db2.Integer, primary_key=True)
    des_cotizacion = db2.Column(db2.Text, nullable=False)
    respuesta = db2.Column(db2.Text, nullable=True)
    id_usuario = db2.Column(db2.Integer, ForeignKey('usuario.id_usuario'), nullable=False)

@dataclass
class Bitacora(base, db2.Model):

    __tablename__ = 'bitacora'
    id_bitacora = db2.Column(db2.Integer, primary_key=True)
    nombre_accion = db2.Column(db2.String(255), nullable=False)
    origen = db2.Column(db2.String(100), nullable=False)
    fecha = db2.Column(db2.Date, nullable=False)
    id_usuario = db2.Column(db2.Integer, ForeignKey('usuario.id_usuario'), nullable=False)


@dataclass
class Ventas(base, db2.Model):

    __tablename__ = 'venta'
    id_venta = db2.Column(db2.Integer, primary_key=True)
    id_producto = db2.Column(db2.Integer, ForeignKey('producto.id_producto'), nullable=False)
    ventas_mes = db2.Column(db2.Integer, nullable=False)
    fecha = db2.Column(db2.Date, nullable=False)


Session = sessionmaker(engine)
sessiondb = Session()